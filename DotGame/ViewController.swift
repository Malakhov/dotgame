//
//  ViewController.swift
//  DotGame
//
//  Created by Dmitry Malakhov on 11/10/16.
//  Copyright © 2016 Dmytro Malakhov. All rights reserved.
//

import UIKit

protocol ViewControllerProtocol: class {
    func updateBoardView() -> ()
}

class ViewController: UIViewController, ViewControllerProtocol {
    
    var gameController: GameController!
    var cellViews: [[CellView]] = []
    var playerRed: UIImageView!
    var playerBlue: UIImageView!
    
    @IBOutlet weak var blueScore: UILabel!
    @IBOutlet weak var redScore: UILabel!
    @IBOutlet weak var gameBoardView: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()
        gameController = GameController()
        gameController.viewController = self
        let cellSize = 300 / CGFloat(gameController.size.width)
        for i in 0..<gameController.size.width {
            var row: [CellView] = []
            for j in 0..<gameController.size.height {
                let xOffset = (view.bounds.width - 300)/2
                let yOffset = (view.bounds.height - 300)/2
                let cellFrame = CGRect(x: CGFloat(i) * cellSize + xOffset, y: CGFloat(j) * cellSize + yOffset, width: cellSize, height: cellSize)
                let cell = CellView(cell: gameController.gameBoard.cells[i][j], frame: cellFrame)
                row.append(cell)
                view.addSubview(cell)
            }
            cellViews.append(row)
        }
        
        let red = #imageLiteral(resourceName: "red")
        let blue = #imageLiteral(resourceName: "blue")
        playerRed = UIImageView(image: red)
        playerBlue = UIImageView(image: blue)
        view.addSubview(playerRed)
        view.addSubview(playerBlue)
        
        playerRed.frame = cellViews.first!.first!.frame
        playerBlue.frame = cellViews.last!.last!.frame
        
        blueScore.text = "0"
        redScore.text = "0"
    }
    
    func updateBoardView() {
        
        UIView.animate(withDuration: 0.2) { 
            for cell in self.cellViews.flatMap({ $0 }) {
                cell.updateState()
            }
            
            let playerRedPosition = self.gameController.gameBoard.players.first!.position
            self.playerRed.center = self.cellViews[playerRedPosition.x][playerRedPosition.y].center
            
            let playerBluePosition = self.gameController.gameBoard.players.last!.position
            self.playerBlue.center = self.cellViews[playerBluePosition.x][playerBluePosition.y].center
            
            self.redScore.text = String(self.gameController.gameBoard.players.first!.score)
            self.blueScore.text = String(self.gameController.gameBoard.players.last!.score)
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}
