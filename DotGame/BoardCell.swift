//
//  BoardCell.swift
//  3DRGame
//
//  Created by Dmitry Malakhov on 11/10/16.
//  Copyright © 2016 Dmytro Malakhov. All rights reserved.
//

import Foundation

enum BoardCellState {
    case empty
    case trail(Player)
    case prize
}

final class BoardCell {
    
    var state: BoardCellState
    let x: Int
    let y: Int

    init(_ state: BoardCellState, x: Int, y: Int) {
        self.x = x
        self.y = y
        self.state = state
    }
}
