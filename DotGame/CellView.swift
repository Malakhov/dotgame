//
//  CellView.swift
//  3DRGame
//
//  Created by Dmitry Malakhov on 11/10/16.
//  Copyright © 2016 Dmytro Malakhov. All rights reserved.
//

import UIKit

class CellView: UIView {
    
    var cell: BoardCell!
    
    convenience init(cell: BoardCell, frame: CGRect) {
        self.init(frame:frame)
        
        self.cell = cell
        switch cell.state {
        case .empty:
            self.backgroundColor = UIColor.lightGray
            self.layer.cornerRadius = frame.width/2
        case .trail(let player):
            self.backgroundColor = player.color
            self.layer.cornerRadius = frame.width/2
        case .prize:
            let prize = #imageLiteral(resourceName: "prize")
            let imageView = UIImageView(image: prize)
            imageView.bounds.size = frame.size
            self.addSubview(imageView)
        }
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func updateState() {
        switch cell.state {
        case .empty:
            self.backgroundColor = UIColor.lightGray
            self.layer.cornerRadius = frame.width/2
            for view in self.subviews {
                view.removeFromSuperview()
            }
        case .trail(let player):
            self.backgroundColor = player.color
            self.layer.cornerRadius = frame.width/2
            for view in self.subviews {
                view.removeFromSuperview()
            }
        case .prize:
            if subviews.count == 0 {
                self.backgroundColor = UIColor.lightGray
                let prize = #imageLiteral(resourceName: "prize")
                let imageView = UIImageView(image: prize)
                imageView.bounds.size = frame.size
                self.addSubview(imageView)
            }
        }
    }

}
