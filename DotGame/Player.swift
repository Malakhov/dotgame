//
//  Player.swift
//  3DRGame
//
//  Created by Dmitry Malakhov on 11/10/16.
//  Copyright © 2016 Dmytro Malakhov. All rights reserved.
//

import Foundation
import UIKit

final class Player {
    
    var position: (x: Int, y:Int)
    let color: UIColor
    private var _score: Int = 0
    var score: Int {
        return _score
    }
    
    init(x: Int, y: Int, color: UIColor) {
        self.position = (x: x, y: y)
        self.color = color
    }
    
    func playerWin() {
        _score += 1
    }
}
