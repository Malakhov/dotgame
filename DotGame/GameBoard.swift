//
//  GameBoard.swift
//  3DRGame
//
//  Created by Dmitry Malakhov on 11/10/16.
//  Copyright © 2016 Dmytro Malakhov. All rights reserved.
//

import Foundation
import UIKit

final class GameBoard {
    
    var cells: [[BoardCell]] = [[BoardCell]]()
    var height: Int = 0
    var width: Int = 0
    var players: [Player] = []
    
    init(width: Int, height: Int) {
        self.cells = generateEmptyBoard(width: width, height: height)
        self.height = height
        self.width = width
        self.cells = addPrize(cells: self.cells)
        self.players = addPlayers()
    }
    
    func reset() {
        for i in 0..<width {
            for j in 0..<height {
                cells[i][j].state = .empty
            }
        }
        players.first!.position = (x: 0,y: 0)
        players.last!.position = (x: (width-1),y: (height - 1))
        cells = addPrize(cells: cells)
        
    }
    
    private func generateEmptyBoard(width: Int, height: Int) -> [[BoardCell]] {
        
        var cells = [[BoardCell]]()
        
        for i in 0..<width {
            var row = [BoardCell]()
            for j in 0..<height {
                row.append(BoardCell(.empty, x: i,y: j))
            }
            cells.append(row)
        }
        
        return cells
    }
    
    private func addPrize(cells: [[BoardCell]]) -> [[BoardCell]] {
        
        let prizeX = Int(arc4random_uniform(UInt32(width)))
        var prizeY = Int(arc4random_uniform(UInt32(height)))
        
        if prizeX == 0 || prizeX == width {
            while prizeY == 0 || prizeY == height {
                prizeY = Int(arc4random_uniform(UInt32(height)))
            }
        }
        
        cells[prizeX][prizeY].state = .prize
        
        return cells
    }
    
    private func addPlayers() -> [Player] {
        
        let playerRed = Player(x: 0,y: 0, color: .red)
        let playerBlue = Player(x: (width-1),y: (height - 1), color: .blue)
        
        return [playerRed, playerBlue]
    }
}
