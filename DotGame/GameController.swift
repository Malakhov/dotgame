//
//  GameController.swift
//  3DRGame
//
//  Created by Dmitry Malakhov on 11/10/16.
//  Copyright © 2016 Dmytro Malakhov. All rights reserved.
//

import Foundation

final class GameController {
    
    weak var viewController: ViewControllerProtocol?
    var gameBoard: GameBoard!
    var timer: Timer?
    let size = (width: 5, height: 5)
    
    init() {
        self.gameBoard = GameBoard(width: size.width, height: size.height)
        start()
    }
    
    func start() {
        timer = Timer.scheduledTimer(timeInterval: 0.5,
                                     target: self,
                                     selector: #selector(GameController.step),
                                     userInfo: nil,
                                     repeats: true)
    }
    
    func restart() {
        self.gameBoard.reset()
    }
    
    @objc private func step() {
        
        for player in gameBoard.players {
            movePlayer(player)
        }
        viewController?.updateBoardView()
    }
    
    private func movePlayer(_ player: Player) {
        
        let allMoves: [BoardCell?] = [possibleCell(player: player, x: player.position.x - 1, y: player.position.y),
                                     possibleCell(player: player, x: player.position.x + 1, y: player.position.y),
                                     possibleCell(player: player, x: player.position.x, y: player.position.y - 1),
                                     possibleCell(player: player, x: player.position.x, y: player.position.y + 1)]
        
        let possibleMoves: [BoardCell] = allMoves.flatMap{ $0 }
        
        if possibleMoves.isEmpty {
            return
        }
        
//If we will move to empty cells first then we can stuck in a corner where all cells around is colored and only one is empty.
        
//        let movesToEmptyCells: [BoardCell] = possibleMoves.filter { (cell) -> Bool in
//            if case .empty = cell.state {
//                return true
//            }
//            return false
//        }
        
//        if !movesToEmptyCells.isEmpty {
//            let cell = movesToEmptyCells[Int(arc4random_uniform(UInt32(movesToEmptyCells.count)))]
//            moveToCell(player: player, cell: cell)
//        }
//        else {
            let cell = possibleMoves[Int(arc4random_uniform(UInt32(possibleMoves.count)))]
            moveToCell(player: player, cell: cell)
//        }
    }
    
    private func possibleCell(player: Player, x: Int, y: Int) -> BoardCell? {
        
        if x < 0 || x >= gameBoard.width || y < 0 || y >= gameBoard.height {
            return nil
        }
        
        switch gameBoard.cells[x][y].state {
        case .trail(let otherPlayer) where otherPlayer !== player:
            return nil
        default:
            return gameBoard.cells[x][y]
        }
    }
    
    private func moveToCell(player: Player, cell: BoardCell) {
        
        if case .prize = cell.state {
            player.playerWin()
            restart()
            return
        }
        
        
        if case .trail(_) = cell.state {
            gameBoard.cells[player.position.x][player.position.y].state = .empty
        }
        else {
            cell.state = .trail(player)
            gameBoard.cells[player.position.x][player.position.y].state = .trail(player)
        }
        player.position = (x: cell.x, y: cell.y)
    }
    
}
